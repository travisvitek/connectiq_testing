#!/bin/bash

monkeyc -v

if [ $? -ne 0 ]; then
    echo "'monkeyc' is not in the PATH." 1>&2
    exit 99 
fi

set -o pipefail

rez=$(find resources resources-* -name "*.xml" | paste -d: -s)
src=$(find source -name "*.mc")

monkeyc \
  --api-version 2.3.4 \
  --manifest manifest.xml \
  --output bin/TestApp.iq \
  --package-app \
  --private-key developer.key \
  --rez ${rez} \
  --warn \
  ${src} 2>&1 | tee build.out

status=$?
if [ $status -ne 0 ]; then
    echo "Build exited with $status" 1>&2
    exit 1
fi

status=$(grep "^ERROR:" build.out | wc -l)
if [ $status -ne 0 ]; then
    echo "$status build error(s)" 1>&2
    exit 1
fi

status=$(grep "^WARNING:" build.out | wc -l)
if [ $status -ne 0 ]; then
    echo "$status build warning(s)" 1>&2
    exit 1
fi

