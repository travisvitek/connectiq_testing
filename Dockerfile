FROM openjdk:8-jre-slim

ARG SDK_VERSION=2.3.4

LABEL maintainer.email="travis.vitek@gmail.com" \
  maintainer.name="Travis Vitek"

RUN apt-get -yqq update && apt-get -yqq install \
  curl \
  zip

RUN mkdir -p /usr/local/connectiq-${SDK_VERSION} && \
  curl -s -o /tmp/connectiq-sdk-linux-${SDK_VERSION}.zip https://developer.garmin.com/downloads/connect-iq/sdks/connectiq-sdk-linux-${SDK_VERSION}.zip && \
  unzip -q -d /usr/local/connectiq-${SDK_VERSION} /tmp/connectiq-sdk-linux-${SDK_VERSION}.zip && \
  rm -rf /tmp/connectiq-sdk-linux-${SDK_VERSION}.zip

ENV PATH ${PATH}:/usr/local/connectiq-${SDK_VERSION}/bin

WORKDIR /build

COPY . /build
COPY ./developer.key.example /build/developer.key

CMD [ "./build_package.sh" ]
