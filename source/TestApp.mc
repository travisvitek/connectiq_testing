using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.Test as Test;
using Toybox.Lang as Lang;

(:test)
function test_Test_assert(logger) {

    var inputs = [
        true,
        false,
        0,
        1,
        0l,
        1l
    ];

    var failures = 0;
    for (var i = 0; i < inputs.size(); ++i) {
        var input = inputs[i];

        var expect_assertion = input == false;

        try {
            Test.assert(input);
            failures += (expect_assertion ? 1 : 0);
        }
        catch (ex instanceof Test.AssertException) {
            failures += (expect_assertion ? 0 : 1);
            // TODO: verify message
        }
    }

    return failures == 0;
}

(:test)
function test_Test_assertEqual(logger) {

    var inputs = [
        true,
        false,
        0,
        0,
        1,
        0l,
        1l,
        0.0,
        1.0,
        0.0d,
        1.0d,
        "a",
        "a",
        "b"
    ];

    var failures = 0;
    for (var i = 0; i < inputs.size(); ++i) {
        var input_i = inputs[i];

        for (var j = 0; j < inputs.size(); ++j) {
            var input_j = inputs[j];

            var expect_assertion = !input_i.equals(input_j);

            try {
                Test.assertEqual(input_i, input_j);
                failures += (expect_assertion ? 1 : 0);
            }
            catch (ex instanceof Test.AssertException) {
                failures += (expect_assertion ? 0 : 1);
                // TODO: verify message
            }
        }
    }

    return failures == 0;
}

(:test)
function test_Test_assertEqualMessage(logger) {

    var inputs = [
        true,
        false,
        0,
        0,
        1,
        0l,
        1l,
        0.0,
        1.0,
        0.0d,
        1.0d,
        "a",
        "a",
        "b"
    ];

    var message = "test";
    var failures = 0;
    for (var i = 0; i < inputs.size(); ++i) {
        var input_i = inputs[i];

        for (var j = 0; j < inputs.size(); ++j) {
            var input_j = inputs[j];

            var expect_assertion = !input_i.equals(input_j);

            try {
                Test.assertEqualMessage(input_i, input_j, message);
                failures += (expect_assertion ? 1 : 0);
            }
            catch (ex instanceof Test.AssertException) {
                failures += (expect_assertion ? 0 : 1);
                // TODO: verify message
            }
        }
    }

    return failures == 0;
}

(:test)
function test_Test_assertMessage(logger) {

    var inputs = [
        true,
        false,
        0,
        1,
        0l,
        1l
    ];

    var message = "test";
    var failures = 0;
    for (var i = 0; i < inputs.size(); ++i) {
        var input = inputs[i];

        var expect_assertion = input == false;

        try {
            Test.assertMessage(input, message);
            failures += (expect_assertion ? 1 : 0);
        }
        catch (ex instanceof Test.AssertException) {
            failures += (expect_assertion ? 0 : 1);
            // TODO: verify message
        }
    }

    return failures == 0;
}

(:test)
function test_Test_assertNotEqual(logger) {

    var inputs = [
        true,
        false,
        0,
        0,
        1,
        0l,
        1l,
        0.0,
        1.0,
        0.0d,
        1.0d,
        "a",
        "a",
        "b"
    ];

    var failures = 0;
    for (var i = 0; i < inputs.size(); ++i) {
        var input_i = inputs[i];

        for (var j = 0; j < inputs.size(); ++j) {
            var input_j = inputs[j];

            var expect_assertion = input_i.equals(input_j);

            try {
                Test.assertNotEqual(input_i, input_j);
                failures += (expect_assertion ? 1 : 0);
            }
            catch (ex instanceof Test.AssertException) {
                failures += (expect_assertion ? 0 : 1);
                // TODO: verify message
            }
        }
    }

    return failures == 0;
}

(:test)
function test_Test_assertNotEqualMessage(logger) {

    var inputs = [
        true,
        false,
        0,
        0,
        1,
        0l,
        1l,
        0.0,
        1.0,
        0.0d,
        1.0d,
        "a",
        "a",
        "b"
    ];

    var message = "test";
    var failures = 0;
    for (var i = 0; i < inputs.size(); ++i) {
        var input_i = inputs[i];

        for (var j = 0; j < inputs.size(); ++j) {
            var input_j = inputs[j];

            var expect_assertion = input_i.equals(input_j);

            try {
                Test.assertNotEqualMessage(input_i, input_j, message);
                failures += (expect_assertion ? 1 : 0);
            }
            catch (ex instanceof Test.AssertException) {
                failures += (expect_assertion ? 0 : 1);
                // TODO: verify message
            }
        }
    }

    return failures == 0;
}

class TestView extends Ui.View
{
    function initialize() {
        View.initialize();
    }
}

class TestApp extends App.AppBase
{
    function initialize() {
        AppBase.initialize();
    }

    function getInitialView() {
        return null;
    }
}
